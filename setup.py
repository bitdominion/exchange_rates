
from setuptools import setup

requires = [
    "Django",
    "django-suit",
    "requests"
]

setup(
    name='BD-Analytics',
    version="0.1.0",
    url='http://www.bitdominion.com/',
    author='Bit Dominion',
    author_email='artlucas@gmail.com',
    license='Proprietary',
    packages=["forex_analytics"],
    include_package_data=True,
    install_requires=requires,
    # scripts=['django/bin/django-admin.py'],
    # entry_points={'console_scripts': [
    #     'manage = django.core.management:execute_from_command_line',
    # ]},
    zip_safe=False,
)
