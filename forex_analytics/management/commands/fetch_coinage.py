import requests

from datetime import datetime
from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand, CommandError

from forex_analytics.models import Market, Rate, Time

API_ENDPOINT = "https://api.coinage.ph/trade/ticker"


class Command(BaseCommand):
    args = "<market_name> [<api_url>]"
    help = """\
Fetches the latest bid/ask prices from Coinage.ph.
    
<api_url> defaults to "{}"
""".format(API_ENDPOINT)

    def handle(self, *args, **options):

        if len(args) == 0 or len(args) > 2:
            raise CommandError("You must specify a market name. Run with --help argument to see help.")

        try:
            market = Market.objects.get(name=args[0])
        except Market.DoesNotExist as e:
            raise CommandError("Cannot find the specified market -- " + repr(args[0]) + " Market names are case sensitive.")
        
        if len(args) == 2:
            api_endpoint = args[1]
        else:
            api_endpoint = API_ENDPOINT

        self.stdout.write(repr(market))

        # Determine the latest timestamp that we already have imported (if any)
        last_rate = Rate.objects.filter(market=market).order_by("-time__date", "-time__time").first()
        
        api_request = requests.get(api_endpoint)
        request_data = api_request.json()
        
        if not request_data or not request_data.get("last"):
            raise CommandError("Couldn't find any rate info from the endpoint. Check " + api_endpoint + " in your browser.")
        
        latest_rate = Decimal(request_data["last"]["value"])
        latest_time = datetime.strptime(request_data["last"]["timestamp"], "%Y-%m-%d %H:%M:%S")
        
        if not last_rate or (latest_time - last_rate.time.to_datetime()).seconds > 3600:
            
            rate = Rate()
            rate.market = market
            rate.time = Time.get_or_create(latest_time)
            rate.amount = latest_rate
            rate.raw_data = repr(request_data)
            self.stdout.write("Saving rate: " + str(rate))
            rate.save()
            
        else:
            
            last_rate.amount = (last_rate.amount + latest_rate) / 2
            
            if last_rate.raw_data:
                raw_data = last_rate.raw_data + "\n" + repr(request_data)
            else:
                raw_data = repr(request_data)
                
            last_rate.raw_data = raw_data
            self.stdout.write("Updating rate: " + str(last_rate))
            last_rate.save()
