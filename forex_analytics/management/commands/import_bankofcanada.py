import csv

from datetime import datetime
from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

from forex_analytics.models import Market, Rate, Time

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--include-historical',
            action='store_true',
            dest='include-historical',
            default=False,
            help='Import older rates, not just ones newer than what we currently have.'),
        )

    args = "<market_name> <file.csv>"
    help = "Imports CSV extracts from Bank of Canada (such as at: http://www.bankofcanada.ca/rates/exchange/10-year-converter/)"

    def handle(self, *args, **options):

        if len(args) != 2:
            raise CommandError("You must specify a market name and a CSV file. Run with --help argument to see help.")

        try:
            market = Market.objects.get(name=args[0])
        except Market.DoesNotExist as e:
            raise CommandError("Cannot find the specified market -- " + repr(args[0]) + " Market names are case sensitive.")

        self.stdout.write(repr(market))

        # Determine the latest timestamp that we already have imported (if any)
        latest_rate = Rate.objects.filter(market=market).order_by("-time__date", "-time__time").first()
        header_row_found = False

        with open(args[1], "rb") as csv_file:
            csv_reader = csv.reader(csv_file)
            
            for row in csv_reader:
                if not header_row_found:
                    if row[0] == "Date" and row[2] == "Conversion rate":
                        header_row_found = True
                    continue

                if not len(row):
                    continue

                rate_date = datetime.strptime(row[0], "%Y-%m-%d").date()
                rate_amount = None

                try:
                    rate_amount = Decimal(row[2])
                except InvalidOperation:
                    pass

                if rate_amount and (not latest_rate or options["include-historical"] or rate_date > latest_rate.time.date):
                    rate = Rate()
                    rate.market = market
                    rate.time = Time.get_or_create(rate_date)
                    rate.amount = rate_amount
                    rate.raw_data = repr(row)
                    self.stdout.write("Saving rate: " + str(rate))
                    rate.save()
