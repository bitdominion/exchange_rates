import csv

from datetime import datetime
from decimal import Decimal
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Min, Max
from optparse import make_option
from time import mktime

from forex_analytics.models import Market, Rate, Time


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--include-historical',
            action='store_true',
            dest='include-historical',
            default=False,
            help='Import older rates, not just ones newer than what we currently have.'),
        )
    
    args = "<market_name> <file.csv>"
    help = "Imports CSV extracts from bitcoincharts.com"

    def handle(self, *args, **options):

        if len(args) != 2:
            raise CommandError("You must specify a market name and a CSV file. Run with --help argument to see help.")

        try:
            market = Market.objects.get(name=args[0])
        except Market.DoesNotExist as e:
            raise CommandError("Cannot find the specified market -- " + repr(args[0]) + " Market names are case sensitive.")

        self.stdout.write(repr(market))

        # Determine the latest timestamp that we already have imported (if any)
        latest_timestamp = 0    
        latest_rate = Rate.objects.filter(market=market).order_by("-time__date", "-time__time").first()

        with open(args[1], "rb") as csv_file:
            csv_reader = csv.reader(csv_file)
            parent_row = csv_reader.next()

            try:

                while parent_row is not None:

                    rate_timestamp = int(parent_row[0])
                    rate_datetime = datetime.fromtimestamp(rate_timestamp)

                    if (not latest_rate or 
                        (rate_datetime.date() > latest_rate.date and 
                        ((latest_rate.time and rate_datetime.time() > latest_rate.time) or latest_rate.time is None)) or
                        options["include-historical"]):

                        grouped_rates = [parent_row]
                        child_row = csv_reader.next()

                        while child_row is not None:
                            child_rate_datetime = \
                                datetime.fromtimestamp(int(child_row[0]))

                            if (child_rate_datetime.date() == rate_datetime.date() 
                                and child_rate_datetime.hour == rate_datetime.time().hour):
                                grouped_rates.append(child_row)
                                child_row = csv_reader.next()
                            else:
                                break

                        avg_amount = sum([Decimal(x[1]) for x in grouped_rates]) / len(grouped_rates)

                        # print "grouped:", repr(grouped_rates)
                        # print "dates (parent, next):", rate_datetime, " --- ", child_rate_datetime
                        # print "avg_amount:", avg_amount
                        # print "-------------"

                        rate = Rate()
                        rate.market = market
                        rate.time = Time.get_or_create(rate_datetime)
                        rate.amount = avg_amount
                        rate.raw_data = repr(grouped_rates)
                        self.stdout.write("Saving rate: " + str(rate))
                        rate.save()

                        parent_row = child_row

                    else:

                        parent_row = csv_reader.next()

            except StopIteration:
                pass
