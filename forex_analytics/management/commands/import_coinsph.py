import json

from datetime import datetime
from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

from forex_analytics.models import Market, Rate, Time


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--include-historical',
            action='store_true',
            dest='include-historical',
            default=False,
            help='Import older rates, not just ones newer than what we currently have.'),
        )

    args = "<market_name> <file.json>"
    help = "Imports JSON extracts from Coins.PH"

    def handle(self, *args, **options):

        if len(args) != 2:
            raise CommandError("You must specify a market name and a JSON file. Run with --help argument to see help.")

        try:
            market = Market.objects.get(name=args[0])
        except Market.DoesNotExist as e:
            raise CommandError("Cannot find the specified market -- " + repr(args[0]) + " Market names are case sensitive.")

        self.stdout.write(repr(market))

        # Determine the latest timestamp that we already have imported (if any)
        latest_rate = Rate.objects.filter(market=market).order_by("-time__date", "-time__time").first()

        with open(args[1], "rb") as json_file:
            price_history = json.load(json_file)

            if not price_history.get("success"):
                raise CommandError("We don't have the success flag -- check the downloaded file.")
            
            for row in price_history["history"]:

                rate_datetime = datetime.strptime(row["timestamp"], "%Y-%m-%d %H:%M:%S")
                rate_amount = None

                try:
                    rate_amount = Decimal(row["ask"])
                except InvalidOperation:
                    pass

                if rate_amount and (not latest_rate or options["include-historical"] or rate_datetime > latest_rate.time.to_datetime()):
                    rate = Rate()
                    rate.market = market
                    rate.time = Time.get_or_create(rate_datetime)
                    rate.amount = rate_amount
                    rate.raw_data = repr(row)
                    self.stdout.write("Saving rate: " + str(rate))
                    rate.save()
