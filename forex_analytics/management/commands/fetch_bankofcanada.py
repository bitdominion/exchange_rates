import csv
import requests

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

from datetime import datetime
from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand, CommandError

from forex_analytics.models import Market, Rate, Time

API_ENDPOINT = "http://www.bankofcanada.ca/stats/results//csv"
API_PARAMS = {"rangeType": "range", "rangeValue": "1", "sF": "LOOKUPS_CAD", 
    "lP": "lookup_currency_converter.php", "sR": "2004-09-22", "sTF": "to", 
    "sT": "_3301", "co": "1.00", "dF": "", "dT": ""}

class Command(BaseCommand):
    args = "<market_name>"
    help = "Fetches the latest exchange rate from Bank of Canada. (such as at: http://www.bankofcanada.ca/rates/exchange/10-year-converter/)"

    def handle(self, *args, **options):

        if len(args) != 1:
            raise CommandError("You must specify a market name. Run with --help argument to see help.")

        try:
            market = Market.objects.get(name=args[0])
        except Market.DoesNotExist as e:
            raise CommandError("Cannot find the specified market -- " + repr(args[0]) + " Market names are case sensitive.")

        self.stdout.write(repr(market))

        # Determine the latest timestamp that we already have imported (if any)
        latest_rate = Rate.objects.filter(market=market).order_by("-time__date", "-time__time").first()
        header_row_found = False

        api_request = requests.get(API_ENDPOINT, params=API_PARAMS)
        csv_reader = csv.reader(StringIO(api_request.text))
            
        for row in csv_reader:
            if not header_row_found:
                if row[0] == "Date" and row[2] == "Conversion rate":
                    header_row_found = True
                continue

            if not len(row):
                continue

            rate_date = datetime.strptime(row[0], "%Y-%m-%d").date()
            rate_amount = None

            try:
                rate_amount = Decimal(row[2])
            except InvalidOperation:
                pass

            if rate_amount and (not latest_rate or rate_date > latest_rate.time.date):
                rate = Rate()
                rate.market = market
                rate.time = Time.get_or_create(rate_date)
                rate.amount = rate_amount
                rate.raw_data = repr(row)
                self.stdout.write("Saving rate: " + str(rate))
                rate.save()
