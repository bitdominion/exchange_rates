import json

from datetime import datetime
from decimal import Decimal, InvalidOperation
from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

from forex_analytics.models import Market, Rate, Time


# [
#     1404950400000,        # KEY, use datetime.fromtimestamp(KEY/1000.0)
#     26948.904840000003,   # OPEN
#     27034.26297,          # HIGH
#     26471.852550000003,   # LOW
#     26604.006,            # CLOSE
#     2498.97               # VOLUME (in peso?)
# ]

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--include-historical',
            action='store_true',
            dest='include-historical',
            default=False,
            help='Import older rates, not just ones newer than what we currently have.'),
        )

    args = "<market_name> <file.json>"
    help = "Imports JSON extracts from Coinage.ph (https://coinage.ph/charts/phpbitcoinchart.json)"

    def handle(self, *args, **options):

        if len(args) != 2:
            raise CommandError("You must specify a market name and a JSON file. Run with --help argument to see help.")

        try:
            market = Market.objects.get(name=args[0])
        except Market.DoesNotExist as e:
            raise CommandError("Cannot find the specified market -- " + repr(args[0]) + " Market names are case sensitive.")

        self.stdout.write(repr(market))

        # Determine the latest timestamp that we already have imported (if any)
        latest_rate = Rate.objects.filter(market=market).order_by("-time__date", "-time__time").first()

        with open(args[1], "rb") as json_file:
            price_history = json.load(json_file)

            if not len(price_history):
                raise CommandError("We don't have the success flag -- check the downloaded file.")
            
            for row in price_history:

                rate_amount = None
                rate_datetime = None

                try:
                    high = Decimal(row[2])
                    low = Decimal(row[3])
                    rate_amount = (high + low) / 2
                    rate_datetime = datetime.fromtimestamp(int(row[0]) / 1000.0)
                except InvalidOperation, ValueError:
                    pass

                if rate_amount and rate_datetime and (not latest_rate or options["include-historical"] or rate_datetime > latest_rate.time.to_datetime()):
                    rate = Rate()
                    rate.market = market
                    rate.time = Time.get_or_create(rate_datetime)
                    rate.amount = rate_amount
                    rate.raw_data = repr(row)
                    self.stdout.write("Saving rate: " + str(rate))
                    rate.save()
