from django.core.management.base import BaseCommand, CommandError
from django.db.models import Min, Max
from forex_analytics.models import Market

class Command(BaseCommand):
    help = "Operations involving markets -- currently just lists them all"

    def handle(self, *args, **options):
        self.stdout.write("{0} {1} {2} {3} {4}".format(
            "COUNT".ljust(8),
            "EARLIEST_DATE".ljust(16),
            "LATEST_DATE".ljust(16),
            "LAST_PRICE".ljust(16),
            "NAME"
        ))
        
        for market in Market.objects.all():
            # <count>   <earliest date>     <latest date>   <name>
            rate_aggregates = market.rate_set.aggregate(Min("time__date"), Max("time__date"))
            latest_rate = market.rate_set.order_by("-time__date", "-time__time").first()
            latest_amount = 0

            if latest_rate:
                latest_amount = latest_rate.amount

            self.stdout.write("{0} {1} {2} {3} {4}".format(
                str(market.rate_set.count()).ljust(8),
                str(rate_aggregates["time__date__min"]).ljust(16),
                str(rate_aggregates["time__date__max"]).ljust(16),
                str(latest_amount).ljust(16),
                market.name
            ))
