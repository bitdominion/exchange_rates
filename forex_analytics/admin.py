
from django.conf.urls import patterns, url
from django.contrib import admin
from django.contrib.admin.templatetags.admin_urls import add_preserved_filters
from django.http import HttpResponse
from django.shortcuts import render
from functools import update_wrapper
from urllib import urlencode

from forex_analytics.models import Market, Rate


class MarketAdmin(admin.ModelAdmin):
	change_form_template = "market_change_form.html"
	list_display = ("name", "rate_count", "last_amount", "last_time")

	def rate_count(self, obj):
		return obj.rate_set.count()
	rate_count.short_description = "Sample Size"

	def last_amount(self, obj):
		rate = obj.rate_set.order_by("time__date", "time__time").last()
		if rate:
			return rate.amount
		return "Nothing yet..."
	last_amount.short_description = "Latest Rate"

	def last_time(self, obj):
		rate = obj.rate_set.order_by("time__date", "time__time").last()
		if rate:
			return rate.time
		return "Nothing yet..."
	last_amount.short_description = "Last Updated"

	def get_urls(self):
		def wrap(view):
			def wrapper(*args, **kwargs):
				return self.admin_site.admin_view(view)(*args, **kwargs)
			return update_wrapper(wrapper, view)
 
		info = self.model._meta.app_label, self.model._meta.model_name
 
		urls = patterns('',
			url(r'^(.+)/graph.html$',
				wrap(self.graph_view),
				name='%s_%s_graph' % info),
		)
 
		super_urls = super(MarketAdmin, self).get_urls()
 
		return urls + super_urls

	def graph_view(self, request, market_id, form_url='', extra_context=None):
		obj = Market.objects.get(id=market_id)
		context = {
			"title": "Graph",
			"pairs": [],
			"original": obj,
			"markets": Market.objects.all()
		}

		params = {}

		if request.REQUEST.get("start_date"):
			params["start_date"] = request.REQUEST["start_date"]

		if request.REQUEST.get("end_date"):
			params["end_date"] = request.REQUEST["end_date"]

		if request.REQUEST.get("markets"):
			params["markets"] = ",".join(request.REQUEST.getlist("markets"))

		if request.REQUEST.get("pairs"):
			params["pairs"] = ",".join(request.REQUEST.getlist("pairs"))

		if not params:
			context["data_url"] = "/data.csv?" + urlencode({"markets": obj.name})
		else:
			context["data_url"] = "/data.csv?" + urlencode(params)

		for cad_market in Market.objects.filter(name__iendswith="BTCCAD"):
			for php_market in Market.objects.filter(name__iendswith="PHPBTC"):
				context["pairs"].append(php_market.name + "/" + cad_market.name)

		opts = Market._meta
		preserved_filters = self.get_preserved_filters(request)
		form_url = add_preserved_filters({'preserved_filters': preserved_filters, 'opts': opts}, form_url)

		if extra_context:
			context.update(extra_context)

		context["form_url"] = form_url
		context["opts"] = opts
		context["app_label"] = opts.app_label
		context["params"] = params

		print context

		return render(request, "graph.html", context)


admin.site.register(Market, MarketAdmin)

class RateAdmin(admin.ModelAdmin):
	#date_hierarchy = "time"
	list_display = ("market", "amount", "time")
	view_on_site = False
	readonly_fields = ("raw_data", )
	ordering = ("-time__date", "-time__time", "market")
	list_per_page = 500
	list_filter = ("market", )

admin.site.register(Rate, RateAdmin)
