from django.conf.urls import patterns, include, url
from django.contrib import admin

from forex_analytics import views


urlpatterns = patterns("",
    url(r"^data.csv$", views.data_extract),
    url(r"", include(admin.site.urls)),
)
