from datetime import datetime, date, time
from django.db import models
from django.contrib import admin

class Market(models.Model):
	name = models.CharField(max_length=16)

	def __unicode__(self):
		return self.name

class Time(models.Model):
	date = models.DateField()
	time = models.TimeField(null=True, blank=True)

	def save(self, *a, **kw):
		if self.time:
			self.time = time(self.time.hour)

		return super(Time, self).save(*a, **kw)

	def __unicode__(self):
		if self.time:
			return datetime.combine(self.date, self.time).ctime()
		return self.date.ctime()
	
	def to_datetime(self):
		time = self.time
		
		if not time:
			time = datetime.min.time()
			
		return datetime.combine(self.date, time)
		

	@staticmethod
	def get_or_create(date_or_datetime, time_=None):

		if isinstance(date_or_datetime, datetime):
			assert time_ is None, "`time_` must be None if `date_or_datetime` is a `datetime.datetime`"
			lookup_date = date_or_datetime.date()
			lookup_time = time(date_or_datetime.hour)
		else:
			lookup_date = date_or_datetime
			lookup_time = None

		if time_ is not None:
			lookup_time = time(time_.hour)

		return Time.objects.get_or_create(date=lookup_date, time=lookup_time)[0]

class Rate(models.Model):
	market = models.ForeignKey(Market)
	time = models.ForeignKey(Time)
	amount = models.DecimalField(max_digits=16, decimal_places=8)
	raw_data = models.TextField(null=True, blank=True)

	def __unicode__(self):
		return "on {0}, {1} was {2}".format(self.time, self.market.name, self.amount)
