import csv

from django.http import HttpResponse
from datetime import datetime

from forex_analytics.models import Market, Rate, Time


def data_extract(request):

	response = HttpResponse(content_type="text/csv")
	response["Content-Disposition"] = "attachment; filename=\"exported_data.csv\""

	query = Rate.objects \
		.select_related("time") \
		.select_related("market") \
		.order_by("-time__date", "-time__time", "market__name")

	markets = []
	market_pairs = [] # "virtex/coinsph,virtex/buybitcoinph"

	if request.REQUEST.get("pairs"):
		market_pairs = [tuple(x.split("/")) for x in request.REQUEST["pairs"].split(",")]

	if request.REQUEST.get("markets"):
		markets = request.REQUEST["markets"].split(",")
		markets.sort()
		#query = query.filter(market__name__in=markets + [x[0] for x in market_pairs] + [x[1] for x in market_pairs])

	if request.REQUEST.get("start_date"):
		start_date = datetime.strptime(request.REQUEST["start_date"], "%Y-%m-%d")
		query = query.filter(time__date__gte=start_date)

	if request.REQUEST.get("end_date"):
		end_date = datetime.strptime(request.REQUEST["end_date"], "%Y-%m-%d")
		query = query.filter(time__date__lte=end_date)

	print query.query

	writer = csv.writer(response)

	writer.writerow(["DATE"] + markets + ["/".join(x) for x in market_pairs])

	current_time = None
	current_time_market_rates = {}

	for row in query:
		print row
		
		if current_time is None:	# if this is None, its our 1st iteration
			current_time = row.time

		# if the time of this row doesn't match our previous, write out what
		# we have so far and begin a new grouping
			
		if str(row.time) != str(current_time):

			for pair in market_pairs:
				if current_time_market_rates.get(pair[0]) and current_time_market_rates.get(pair[1]):
					current_time_market_rates[pair] = \
						current_time_market_rates[pair[0]] / current_time_market_rates[pair[1]]

			formatted_row = format_row(current_time, markets, market_pairs, current_time_market_rates)

			if any(formatted_row[1:]):
				writer.writerow(formatted_row)

			current_time = row.time
			current_time_market_rates = {}

		current_time_market_rates[row.market.name] = row.amount

	# don't forget to write the last row

	formatted_row = format_row(current_time, markets, market_pairs, current_time_market_rates)

	if any(formatted_row[1:]):
		writer.writerow(formatted_row)

	return response

def format_row(row_time, markets, market_pairs, market_data):

	market_cols = [""] * len(markets) 
	pair_cols = [""] * len(market_pairs)

	for key, rate in market_data.items():
		try:
			idx = markets.index(key)
			market_cols[idx] = rate
		except ValueError:
			try:
				idx = market_pairs.index(key)
				pair_cols[idx] = rate
			except ValueError:
				pass

	return [row_time] + market_cols + pair_cols
